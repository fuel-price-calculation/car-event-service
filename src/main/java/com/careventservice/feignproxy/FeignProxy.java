package com.careventservice.feignproxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.careventservice.config.FeignConfig;
import com.careventservice.model.RequestModel;

@RibbonClient
@FeignClient(name = "xxiap010-mongo-service", url = "localhost:9091/car", configuration = FeignConfig.class)
public interface FeignProxy {

	@PostMapping(value = "/refill-instruction")
	public void refilInstruction(@RequestBody RequestModel request);
}
