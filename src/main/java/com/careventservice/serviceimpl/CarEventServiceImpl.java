package com.careventservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.careventservice.feignproxy.FeignProxy;
import com.careventservice.model.RequestModel;
import com.careventservice.service.CarEventService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@EnableScheduling
public class CarEventServiceImpl implements CarEventService {

	static List<String> cityList;
	private static RequestModel requestModel;
	@Autowired
	private FeignProxy feign;

	static {
		cityList = new ArrayList<>();
		cityList.add("Bangalore");
		cityList.add("Kolkata");
		cityList.add("Delhi");
		cityList.add("Mumbai");
		cityList.add("Chennai");
		cityList.add("Bhopal");
		cityList.add("Hyderabad");
		cityList.add("Bhubaneshwar");
		cityList.add("Shimla");
		cityList.add("Ladakh");
		cityList.add("Kochi");
		cityList.add("Goa");
		cityList.add("Pune");
		cityList.add("Gurgaon");
		cityList.add("Mysuru");
		cityList.add("Patna");
		requestModel = new RequestModel();
		requestModel.setFuelLid(false);

	}

	@Scheduled(fixedDelay = 120000, initialDelay = 5000)
	public void fuelSchedular() {
		if (requestModel.getFuelLid()) {
			requestModel.setFuelLid(false);
		} else {
			requestModel.setFuelLid(true);
			requestModel.setCity(cityList.get(ThreadLocalRandom.current().nextInt(0, 15)));
		}
		log.info(":: Event trigger through Schedular ::");
		triggerEvent(requestModel);

	}

	@Override
	public String triggerEvent(RequestModel request) {
		String status = "failed";
		try {
			log.info(":: Received request for city : {} and lid to open status : {}", request.getCity(),
					request.getFuelLid());
			feign.refilInstruction(request);
			status = "success";
		} catch (Exception e) {
			log.error(":: Exception while calling event: " + e);
		}
		return status;
	}
}
