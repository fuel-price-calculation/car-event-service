package com.careventservice.service;

import com.careventservice.model.RequestModel;

public interface CarEventService {

	public String triggerEvent(RequestModel request);

}
