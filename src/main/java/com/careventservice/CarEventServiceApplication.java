package com.careventservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class CarEventServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarEventServiceApplication.class, args);
	}

}
