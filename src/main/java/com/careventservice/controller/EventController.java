package com.careventservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.careventservice.model.RequestModel;
import com.careventservice.service.CarEventService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/car")
public class EventController {

	@Autowired
	private CarEventService carservice;

	@PostMapping("/event")
	public ResponseEntity<String> triggerEvent(@RequestBody RequestModel request) {
		log.info(":: Received request for city through API ::");
		carservice.triggerEvent(request);
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}

}
